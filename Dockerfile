# syntax=docker/dockerfile-upstream:1-labs

FROM alpine:3.17

EXPOSE 10500
EXPOSE 10504
EXPOSE 10505

LABEL org.opencontainers.image.authors="uak@gitlab"

ENV USER_HOME /home/user
ENV DATA_DIR $USER_HOME/data
ENV ENV_FILE $DATA_DIR/.env

ENV EC_BRANCH "4.4.1"
RUN apk update
RUN apk --no-cache upgrade
RUN apk add --no-cache python3 py3-pip libsecp256k1-dev git 


RUN adduser user --disabled-password --gecos "" --home $USER_HOME/
USER user
WORKDIR $USER_HOME
RUN mkdir $DATA_DIR
VOLUME $DATA_DIR

RUN git clone --depth 1 --branch $EC_BRANCH https://github.com/Electron-Cash/Electron-Cash
WORKDIR $USER_HOME/Electron-Cash
RUN pip3 install -r contrib/requirements/requirements.txt --user
RUN pip3 install ec-slp-lib pexpect

WORKDIR $USER_HOME

COPY . app/

CMD /usr/bin/python3 $USER_HOME/app/src/wallet_logic.py && \
    tail -f /dev/null

