#!/usr/bin/env python3

import os
import time
import logging


from decimal import Decimal


# Import wallet functions
from ec_slp_lib import EcSLP


# Library to fix issue with creating wallets without user interaction
# https://github.com/Electron-Cash/Electron-Cash/issues/2341
from create_wallet_pexpect import create_wallet_pexpect

from helpers import config_dict

# Enable logging
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
    level=logging.DEBUG,  # DEBUG #INFO
)

logger = logging.getLogger(__name__)


# Creaste instance of EcSLP
ec_slp = EcSLP()

# Time between daemon restart and wallet load to allow it to take a breath
sleep_time = int(config_dict["sleep_time"])


def create_non_existing_wallets(config_dict, network):
    """Create wallet for each enabled network if does not exist"""
    wallet_path = config_dict[network]["wallet_path"]
    logger.debug(f"wallet path {wallet_path}")
    if os.path.isfile(wallet_path):
        logger.info(f"create_non_existing_wallets: {wallet_path} exist as file")
        pass
    else:
        python_path = config_dict["python_path"]
        ec_path = config_dict["electron_cash_path"]
        data_dir = config_dict["custom_dir"]
        create_wallet_pexpect(python_path, ec_path, data_dir, network)
        logger.debug(f"wallet created for network {network}")


def handle_config(network):
    """Get configuration data from Electron Cash config file for each network"""
    try:

        if network != "mainnet":
            config_dict["network"] = network

        else:
            config_dict["network"] = "mainnet"

        logger.debug(f"handle_config: {network}")

        ec_slp.set_config_data(config_dict, "rpcport", config_dict[network]["port"])
        ec_slp.set_config_data(config_dict, "rpchost", config_dict["rpchost"])
        ec_slp.set_config_data(
            config_dict, "rpcpassword", config_dict[network]["password"]
        )

        logger.debug(
            f'handle_config: {network} set port {config_dict[network]["port"]} '
        )
        logger.debug(f'handle_config: {network} set rpchost {config_dict["rpchost"]}')
        logger.debug(
            f'handle_config: {network} set rpcpassword length {len(config_dict[network]["password"])} '
        )

    except Exception as e:
        logger.error(
            f"getting config: Unable to get or set config from config file, Exception: {e}"
        )
        raise e


def reset_daemon():
    """Stop and restart daemon"""
    logger.debug("reset_daemon: Restarting daemon to set password in config file")

    ec_slp.stop_daemon(config_dict)
    logger.debug("reset_daemon: stopping daemon")

    time.sleep(sleep_time)
    ec_slp.start_daemon(config_dict)
    time.sleep(sleep_time)


def daemon_start_fn(network):
    try:
        config_dict["network"] = network

        ec_slp.start_daemon(config_dict)
        logger.info("daemon_start_load_wallet: Daemon started")
        time.sleep(sleep_time)
    except Exception as e:
        logger.info(f"Could not start some daemon \n:{e}")
        raise e


def load_wallet_fn(network):
    # Start daemon and load wallet for each enabled network
    try:
        config_dict["network"] = network
        wallet_path = config_dict[network]["wallet_path"]
        logger.debug(f"load_wallet_fn: path: {wallet_path}")
        ec_slp.load_wallet(config_dict, wallet_path)
        logger.info(f"load_wallet_fn: wallet {wallet_path} loaded")
    except Exception as e:
        logger.info(f"load_wallet_fn: Could not load some wallets \n:{e}")
        raise e


def handle_check_daemon():
    """Check daemon status for each wallet"""

    try:
        ec_slp.check_daemon(config_dict)
        logger.info("Daemon is running")
    except Exception as e:
        logger.info(f"could not check daemon running {e}")


def handle_check_wallet_loaded(network):
    """Check that wallets are loaded"""
    config_dict["network"] = network
    try:
        wallet_loaded = ec_slp.check_wallet_loaded(
            config_dict, config_dict[network]["wallet_path"]
        )
        logger.info(f"Wallet for {network} is loaded: {wallet_loaded}")
    except Exception as e:
        logger.info(f"Some wallets are not loaded\n {e}")
        raise e


def handle_rpc_balance_check(network):
    """Check RPC connection for each network and check balance"""
    try:
        logger.debug(f"Network: {network}, RPC info: {config_dict[network]}")

        bch_balance = ec_slp.get_bch_balance(config_dict[network])
        logger.info(
            f"handle_rpc_balance_check: Balance for network {network} is {bch_balance}"
        )
        if config_dict["allow_zero_balance"] is False:
            min_allowed_balance = config_dict["min_allowed_balance"]
            if Decimal(bch_balance) >= Decimal(min_allowed_balance):
                pass
            else:
                logger.info(f"No balance: {bch_balance}")
                exit("No Balance!")
        logger.info(f"Balance for network {network} is {bch_balance}")
    except Exception as e:
        logger.error(
            f"handle_rpc_balance_check: Error connecting to RPC provider, Exception: {e}"
        )
        raise e


for network in config_dict["network_options"]:

    create_non_existing_wallets(config_dict, network)

    daemon_start_fn(network)

    load_wallet_fn(network)

    handle_check_daemon()

    handle_check_wallet_loaded(network)

    handle_config(network)


try:
    for network in config_dict["network_options"]:
        handle_rpc_balance_check(network)
except Exception as e:
    logger.debug(f"Retrying check balance {e}")
    for network in config_dict["network_options"]:
        reset_daemon()
        ec_slp.load_wallet(config_dict, config_dict[network]["wallet_path"])
    handle_rpc_balance_check(network)
